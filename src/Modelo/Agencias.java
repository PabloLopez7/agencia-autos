/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Modelo;

/**
 *
 * @author Usuario
 */
public class Agencias {
    private double agencia1[] = new double[11];
    private double agencia2[] = new double[11];
    private double agencia3[] = new double[11];
    private double agencia4[] = new double[11];
    private double agencia5[] = new double[11];

    public Agencias(double agencia1[],double agencia2[],double agencia3[],double agencia4[],double agencia5[]) {
        this.agencia1 = agencia1;
        this.agencia2 = agencia2;
        this.agencia3 = agencia3;
        this.agencia4 = agencia4;
        this.agencia5 = agencia5;
    }    
    
    public double[] getAgencia1() {
        return agencia1;
    }

    public void setAgencia1(double[] agencia1) {
        this.agencia1 = agencia1;
    }

    public double[] getAgencia2() {
        return agencia2;
    }

    public void setAgencia2(double[] agencia2) {
        this.agencia2 = agencia2;
    }

    public double[] getAgencia3() {
        return agencia3;
    }

    public void setAgencia3(double[] agencia3) {
        this.agencia3 = agencia3;
    }

    public double[] getAgencia4() {
        return agencia4;
    }

    public void setAgencia4(double[] agencia4) {
        this.agencia4 = agencia4;
    }

    public double[] getAgencia5() {
        return agencia5;
    }

    public void setAgencia5(double[] agencia5) {
        this.agencia5 = agencia5;
    }
    
}
